//
//  ImagesModel.swift
//  ImagesInTable
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

struct Country: Codable{
    let title : String?
    let rows : [CountryDetail]?
}

struct CountryDetail: Codable{
    let title : String?
    let description : String?
    let imageHref : String?
}


