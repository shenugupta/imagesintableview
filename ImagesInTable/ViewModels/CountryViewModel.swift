//
//  ImagesListViewModel.swift
//  ImagesInTable
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

enum CountryApiParsingError: Error {
    case invalidUrl
    case invalidEncoding
    case serverError
}

class CountryViewModel {
    let contryInfoUrl = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    var reloadList = {()->() in}
    var errorMessage = {(message:String) ->() in}
    var country: Country? = nil {
        didSet{
            reloadList()
        }
    }

    func getCountryData() throws {
        guard let listURL = URL(string: contryInfoUrl) else {
            throw CountryApiParsingError.invalidUrl
        }
        URLSession.shared.dataTask(with:listURL) {
            (data, response, error) in
            let data = data
            do {
                self.country = try self.parseData(data: data!);
            } catch let error as NSError {
                print("error \(error)")
            }
        }.resume()
    }
    
    func parseData(data: Data)throws -> Country {
        do {
            guard let isoLatin1String = String(data: data, encoding: .isoLatin1) as String? else {
                throw CountryApiParsingError.invalidEncoding
            }
            let dataFromLatin1String = Data(isoLatin1String.utf8)
            let decoder = JSONDecoder()
            return try decoder.decode(Country.self, from: dataFromLatin1String)
        }
        catch {
            throw CountryApiParsingError.serverError
        }
    }
 }





