//
//  Constants.swift
//  ImagesInTable
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit
import Foundation

let RANDOM_USER_URL = "http://api.randomuser.me/?results=10&nat=e"
let GITHUB_URL = ""
let SHADOW_GRY: CGFloat = 120.0 / 255.0


typealias DownloadComplete = () -> ()
