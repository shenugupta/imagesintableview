//
//  CustomImageTableViewCell.swift
//  ImagesInTable
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

class CustomImageTableViewCell: UITableViewCell {
    let profileImageView:UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        //img.layer.cornerRadius = 35
        img.clipsToBounds = true
        return img
    }()
    let title:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let countryDescription:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true // this will make sure its children do not go out of the boundary
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(profileImageView)
        containerView.addSubview(title)
        containerView.addSubview(countryDescription)
        self.contentView.addSubview(containerView)
        profileImageView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor).isActive = true
        profileImageView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant:70).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant:70).isActive = true
        containerView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo:self.profileImageView.trailingAnchor, constant:10).isActive = true
        containerView.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant:-10).isActive = true
        containerView.heightAnchor.constraint(equalToConstant:40).isActive = true
        title.topAnchor.constraint(equalTo:self.containerView.topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo:self.containerView.leadingAnchor).isActive = true
        title.trailingAnchor.constraint(equalTo:self.containerView.trailingAnchor).isActive = true
        countryDescription.topAnchor.constraint(equalTo:self.title.bottomAnchor).isActive = true
        countryDescription.leadingAnchor.constraint(equalTo:self.containerView.leadingAnchor).isActive = true
       countryDescription.topAnchor.constraint(equalTo:self.title.bottomAnchor).isActive = true
        countryDescription.leadingAnchor.constraint(equalTo:self.containerView.leadingAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
  }






