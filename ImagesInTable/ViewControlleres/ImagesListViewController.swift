//
//  ImagesListViewController.swift
//  ImagesInTable
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

class ImagesListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var urlSessionDownloadTask : URLSessionDownloadTask!
    var urlSession: URLSession!
    var cache:NSCache<AnyObject,AnyObject>!
    
    
    
    var viewModel = CountryViewModel()
    let cellReuseIdentifier: String = "imageCell";
    let defaultImage: String = "image1.png";
    @objc let countryTableView = UITableView()
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        
        
        refreshControl.addTarget(self, action: #selector(ImagesListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializations()
        setupViewsAndConstraits()
        getCountry()
    }
    
    func initializations() {
        self.cache = NSCache()
        
        
        
        
    }
    
    func setupViewsAndConstraits() {
        self.countryTableView.addSubview(self.refreshControl)
        
        
        
        countryTableView.estimatedRowHeight = 200
        countryTableView.rowHeight = UITableViewAutomaticDimension
        urlSession = URLSession.shared
        urlSessionDownloadTask =  URLSessionDownloadTask()
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(getter: ImagesListViewController.countryTableView), for: .valueChanged)
        view.addSubview(countryTableView)
        countryTableView.dataSource = self
        countryTableView.register(CustomImageTableViewCell.self, forCellReuseIdentifier:cellReuseIdentifier)
        navigationItem.title = "";
        countryTableView.translatesAutoresizingMaskIntoConstraints = false
        countryTableView.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        countryTableView.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        countryTableView.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        countryTableView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if NetworkConnectivity.isConnectedToInternet() {
        self.countryTableView.reloadData()
        refreshControl.endRefreshing()
        }
          else{
            let alert = UIAlertController(title: "Network Error", message: "Network not Reachable", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
    }
}
        
    func getCountry(){
         if NetworkConnectivity.isConnectedToInternet() {
        do {
            try viewModel.getCountryData()
        }
        catch let e {
            let alert = UIAlertController(title: "Error", message: e.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        clousureSetUp()
        }
         else{
            let alert = UIAlertController(title: "Network Error", message: "Network not Reachable", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func clousureSetUp(){
        if NetworkConnectivity.isConnectedToInternet() {
        viewModel.reloadList = {[weak self] () in
            DispatchQueue.main.async {
                self?.countryTableView.reloadData()
                self?.navigationItem.title = self?.viewModel.country?.title;
            }
        }
        viewModel.errorMessage = { [weak self](message) in
            DispatchQueue.main.async {
            self?.refreshControl.endRefreshing()
            }
        }
      }
        else{
            let alert = UIAlertController(title: "Network Error", message: "Network not Reachable", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        self.countryTableView.layoutSubviews()
        self.clousureSetUp()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.country != nil {
           return (viewModel.country?.rows!.count)!
        }
        else {
            return 0
        }
    }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CustomImageTableViewCell
        let countryDetail = viewModel.country?.rows![indexPath.row]
        cell.title.text = countryDetail?.title
        cell.countryDescription.text = countryDetail?.description
        //self.countryTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        if (self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) != nil){
            print("Cached image used, no need to download it")
            cell.profileImageView.image = self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) as? UIImage
        }else{
            let imageToDownload = countryDetail?.imageHref
            
            if imageToDownload == nil{
                cell.profileImageView.image = UIImage(named:defaultImage)
                print("image is nil")
            }
            else{
                let url:URL! = URL(string: imageToDownload!)
                urlSessionDownloadTask = urlSession.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                    if let data = try? Data(contentsOf: url){
                        DispatchQueue.main.async(execute: { () -> Void in
                            if tableView.cellForRow(at: indexPath) != nil {
                                let img:UIImage! = UIImage(data: data)
                                if img != nil {
                                    self.cache.setObject(img, forKey: (indexPath as NSIndexPath).row as AnyObject)
                                    cell.profileImageView.image = img
                                }
                                else{
                                    cell.profileImageView.image = UIImage(named:self.defaultImage)
                                }
                                
                            }
                        })
                    }
                })
                urlSessionDownloadTask.resume()
            }
        }
        return cell
    }
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
}
