//
//  ImagesInTableTests.swift
//  ImagesInTableTests
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

//
//  ImagesInTableUITests.swift
//  ImagesInTableUITests
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import XCTest
@testable import ImagesInTable
class CountryTests: XCTestCase {
    
    let countryViewModel = CountryViewModel()
    var expectation:XCTestExpectation?
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetCountryData(){
        countryViewModel.reloadList = {[weak self] () in
            self?.expectation?.fulfill()
        }
        self.expectation = self.expectation(description: "parse webservice")
        do {
            try countryViewModel.getCountryData()
        }
        catch let e {
            XCTAssertTrue(false, e.localizedDescription)
        }
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testDecodeCountryJson() {
        let path = Bundle.main.path(forResource: "canada_country", ofType: "json")
        do {
            let jsonString = try String(contentsOfFile:path!, encoding: .isoLatin1)
            let data = jsonString.data(using: .isoLatin1)!
            let country: Country = try countryViewModel.parseData(data: data)
            XCTAssertNotNil(country, "decoder couldn't decode the object")
        } catch let e {
            XCTAssertNil(e, "exception occured")
        }
    }
    
}

