//
//  Image.swift
//  ImagesInTable
//
//  Created by Shenu Gupta on 8/4/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

struct Image{
    let title : String
    let titleDesc : String
}
